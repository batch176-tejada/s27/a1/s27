let http = require("http");

let courses = [

{
    "name" : "JavaScript 101",
    "price" : 5000,
    "description" : "Introduction to JavaScript",
    "isActive" : true,
    "enrollees" : 10.0
},

{
    "name" : "HTML 101",
    "price" : 2000,
    "description" : "Introduction to HTML",
    "isActive" : false,
    "enrollees" : 10.0
},

{
    "name" : "CSS 101",
    "price" : 2500,
    "description" : "Introduction to CSS",
    "isActive" : false,
    "enrollees" : 10.0
}

]





http.createServer(function(req,res){
	

	console.log(req.url);
	console.log(req.method);

	if (req.url === "/" && req.method === "GET"){

			res.writeHead(200,{'Content-Type':'text/plain'});
			res.end("This is a response to GET method request!");
	}
	else if (req.url === "/" && req.method === "POST"){
			res.writeHead(200,{'Content-Type':'text/plain'});
			res.end("This is a response to PosT method request!");
	}
	else if (req.url === "/" && req.method === "PUT"){
			res.writeHead(200,{'Content-Type':'text/plain'});
			res.end("This is a response to PUT method request");	
	}
	else if (req.url === "/" && req.method === "DELETE"){
			res.writeHead(200,{'Content-Type':'text/plain'});
			res.end("This is a response to DELETE method request");
	}

	else if(req.url === "/courses" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(courses));
	}

	let requestBody = "";

		req.on('data',function(data){

		/*	console.log(data);*/
			requestBody += data
		})

		req.on('end',function(){
			console.log(requestBody)
			requestBody = JSON.parse(requestBody);

		let newCourse = {

			name: requestBody.name,
			price: requestBody.price,
			isActive: true		
		}
			console.log(newCourse);
			courses.push(newCourse);
			console.log(courses);
		})

/*	else if(req.url === "/courses" && req.method === "POST"){*/
		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(courses));
	/*}*/

})


.listen(4000);

console.log("Sever is running at localhost:4000");